
# TypeScript + node-gyp (example)

Przykład połączenia projektu [ts-startek-kit](https://bitbucket.org/Adrosar/ts-startek-kit) _(ver. [2.0.0](https://bitbucket.org/Adrosar/ts-startek-kit/src/2.0.0/))_ z modułem natywnym Node.js napisanym w **C/C++**

Materiały edukacyjne:

 - Poradnik ["Rozszerzanie Node.js natywnymi modułami C++"](https://bulldogjob.pl/news/612-rozszerzanie-node-js-natywnymi-modulami-c)
 - To samo co powyżej ale po [angielsku](https://medium.com/@marcinbaraniecki/extending-node-js-with-native-c-modules-63294a91ce4)
 - Repo [node-gyp](https://github.com/nodejs/node-gyp)
 - Repo [nan](https://github.com/nodejs/nan)
 - Repo [uWebSockets.js](https://github.com/uNetworking/uWebSockets.js)
 - Oficjalna [dokumentacja](https://nodejs.org/api/addons.html) - na koniec ponieważ nie jest łatwa w czytaniu :p

---
↓ Poniżej oryginalna treść pliku README.md  z projektu [ts-startek-kit](https://bitbucket.org/Adrosar/ts-startek-kit) _(ver. [2.0.0](https://bitbucket.org/Adrosar/ts-startek-kit/src/2.0.0/))_ ↓

---

Repozytorium jest przeznaczone do budowania:
 - Modułów `node_modules` do późniejszego użytku w aplikacjach **Node.js** lub **Webpack**.
 - Aplikacji dla **Node.js**.
 - Bibliotek dla przeglądarek internetowych.
 - Prostych aplikacji dla przeglądarek internetowych _(bardziej rozbudowane aplikacje polecam budować z użyciem [webpack-starter-kit](https://bitbucket.org/Adrosar/webpack-starter-kit))_.


## ☰ Spis treści
 - Jak zacząć
 - Polecenia
 - Skrypty
    - Aplikacja Node.js
	- Biblioteka dla przeglądarki - rollup
	- Biblioteka dla przeglądarki - browserify
	- Pozostałe polecenia
 - Tworzenie i dystrybucja]
	- Aplikacja lub biblioteka dla Node.js
	- Aplikację lub biblioteka dla przeglądarki]
	- Moduł `node_modules` dla przeglądarki internetowej
- Ustawienia
	- Nazwa biblioteki w przeglądarce


<a name="jak-zaczac"></a>
## ⚙ Jak zacząć

 1. Zainstaluj [Node.js](https://nodejs.org) w wersji **8.11.3** lub nowszej  (zalecam **10.14.x**).
 2. Zainstaluj [npm](https://www.npmjs.com/package/npm) w wersji **5.6.0** lub nowszej _(w systemie Windows **npm** jest instalowane wraz z **Node.js**)_.
 3. Zainstaluj [YARN](https://yarnpkg.com) w wersji **1.2.1** lub nowszej.
 4. Otwórz terminal _(konsola, CMD)_ i przejdź do katalogu projektu.
 5. Wpisz polecenie `yarn install` i poczekaj aż wszystkie zależności zostaną zainstalowane.
 6. Środowisko jest gotowe do pracy ☺.

## ⚙ Polecenia

 - `npm install` / `yarn install` - Instaluje zależności.
 - `npm pack` - Buduje paczkę `*.tar.gz`

_(Tam gdzie jest to możliwe zalecam używać YARN'a)_

## ⚙ Skrypty

Skrypty uruchamiamy poprzez jedną z trzech składni:
 - `npm run NAZWA_SKRYPTU`
 - `yarn run NAZWA_SKRYPTU` ← zalecana składnia
 - `yarn NAZWA_SKRYPTU`

_(Nazwy skryptów wraz z opisem znajdują się poniżej)_

### ❖ Aplikacja Node.js:

Z wykorzystaniem kompilatora **TSC**.

 - `node:build` - Buduje aplikacje do katalogu `./build/app`.
 - `node:launch` - Uruchamia wcześniej zbudowana aplikację. Alias do polecenia `node ./build/app`
 - `node:watch` - Uruchamia tryb _"czuwaj i buduj"_.
 - `node:dist` - Buduje aplikację przeznaczoną do dystrybucji - katalog `./dist`.
 - `node:test` - Buduje i uruchamia testy jednostkowe.
 - `node:test:watch` - Skrypt monitoruje katalogi `./source` i `./test`, a następnie uruchamia testy jednostkowe po każdej zmianie plików _(wykorzystuje aplikacje `nodemon`)_.

### ❖ Biblioteka dla przeglądarki:

Z wykorzystaniem aplikacji budującej **rollup.js** _(korzysta z kompilatora **TSC**)_ i aplikacji minimalizującej pliki **uglifyjs**.

 - `rollup:build` - Buduje jeden plik JavaScript przeznaczony dla przeglądarki.
 - `rollup:watch` - Uruchamia tryb _"czuwaj i buduj"_.
 - `rollup:test` - Buduje jeden plik JavaScript, który zawiera testy jednostkowe i aplikację.
 - `rollup:test:watch` - Skrypt monitoruje katalogi `./source` i `./test`, a następnie buduje testy jednostkowe po każdej zmianie plików _(wykorzystuje aplikacje `nodemon`)_.
 - `rollup:dist` - Buduje dwa pliki JavaScript w wersji produkcyjnej. Pierwszy `./dist/index.js` i drugi zminimalizowany `./dist/index.min.js`. 

### ❖ Biblioteka dla przeglądarki:

Z wykorzystaniem kompilatora **TSC**, aplikacji **browserify** i **uglifyjs**.

 - `browserify:build` - Buduje jeden plik JavaScript przeznaczony dla przeglądarki.
 - `browserify:test` - Buduje jeden plik JavaScript, który zawiera testy jednostkowe i aplikację.
 - `browserify:dist` - Buduje dwa pliki JavaScript w wersji produkcyjnej. Pierwszy `./dist/index.js` i drugi zminimalizowany `./dist/index.min.js`. 

### ❖ Pozostałe polecenia:
- `clear` - Czyści projekt ze zbędnych plików i katalogów.
- `server` - Uruchamia lokalny serwer HTTP.
- `typedoc` - Generuje dokumentację na podstawie kodu źródłowego `./source` i pliku `./README.md`.

## ⚙ Tworzenie i dystrybucja

### ❖ Aplikacja lub biblioteka dla Node.js

 1. Wykonaj wszystkie kroki z punktu **"Jak zacząć"**.
 2. Napisz kod aplikacji zaczynając od pliku `./source.index.ts`.
 3. Napisz testy jednostkowe zaczynając od pliku `./test/index.ts`.
 4. Uruchom testy jednostkowe poleceniem `npm run node:test`.
	 - Jeżeli testy się NIE powiodą wróć do kodu i popraw go.
 5. Zbuduj aplikację w wersji produkcyjnej poleceniem `npm run node:dist`.
 6. Stwórz paczkę do dystrybucji poleceniem `npm pack`.

### ❖ Aplikacja lub biblioteka dla przeglądarki:

 1. Wykonaj wszystkie kroki z punktu **"Jak zacząć"**.
 2. Napisz kod aplikacji zaczynając od pliku `./source/index.ts`.
 3. Napisz testy jednostkowe zaczynając od pliku `./test/index.ts`.
 4. Zbuduj testy jednostkowe poleceniem `npm run rollup:test`.
 5. Uruchom serwer developerski poleceniem `npm run server`.
 6. Otwórz w przeglądarce internetowej adres http://127.0.0.1:60188
 7. Uruchom konsole developerską _(w Google Chrome jest to klawisz **F12**)_.
	 - Przejdź na kartę "Console" (lub "Konsola").
 8. Przejdź pod link [test.rollup.html](http://127.0.0.1:60188/test.rollup.html).
 9. Jeżeli w konsoli nie wyskoczy żaden błąd to znaczy, iż testy przeszły pomyślnie :D
 10. Zbuduj plik do dystrybucji poleceniem `npm run rollup:dist`.

Jeżeli chcesz wykorzystać **browserify** zamiast **rollup** to zamień poniższe punkty:

 - (punkt 4) Zbuduj testy jednostkowe poleceniem `npm run browserify:test`
 - (punkt 8) Przejdź pod link [test.browserify.html](http://127.0.0.1:60188/test.browserify.html)
 - (punkt 10) Zbuduj plik do dystrybucji poleceniem `npm run browserify:dist`

Twoje pliki będą się znajdować w katalogu `./dist`:
 - index.js
 - index.min.js _(zminimalizowana wersja pliku)_
 
 
### ❖ Moduł `node_modules` dla przeglądarki internetowej:

 1. Wykonaj wszystkie kroki z punktu **"Jak zacząć"**.
 2. Napisz kod aplikacji zaczynając od pliku `./source/index.ts`.
 3. Napisz testy jednostkowe zaczynając od pliku `./test/index.ts`.
 4. Zbuduj testy jednostkowe poleceniem `npm run rollup:test`.
 5. Uruchom serwer developerski poleceniem `npm run server`.
 6. Otwórz w przeglądarce internetowej adres http://127.0.0.1:60188
 7. Uruchom konsole developerską _(w Google Chrome jest to klawisz **F12**)_.
	 - Przejdź na kartę "Console" (lub "Konsola").
 8. Przejdź pod link [test.rollup.html](http://127.0.0.1:60188/test.rollup.html).
 9. Jeżeli w konsoli nie wyskoczy żaden błąd to znaczy, iż testy przeszły pomyślnie :D
	 - Pamiętaj aby wykonać testy na wszystkich przeglądarkach internetowych które chcesz wspierać!
 10. Zbuduj moduł **npm** do dystrybucji poleceniem `npm run node:dist`
 11. W zależności od sposobu dystrybucji modułu:
	 - Wyślij zmiany na serwer [bitbucket.org](https://bitbucket.org), [github.com](https://github.com) lub inny wspierający repozytorium GIT.
	 - Zbuduj archiwum `*.tar.gz` poleceniem `npm pack` i wyśnij je do repozytorium modułów [npmjs.com](https://www.npmjs.com)

## ⚙ Ustawienia

### ❖ Nazwa biblioteki w przeglądarce:

**§1 ⇒** W pliku `./package-lock.json` znajdują się parametry dla aplikacji budującej [browserify.org](http://browserify.org):

 - `--standalone __app`
 - `--standalone __test` → używane do uruchomienia testów jednostkowych!

oznaczają one że pod nazwą `__app` w przeglądarce internetowej będzie dostępna dana aplikacja _(lub biblioteka)_.

Jeżeli **usuniemy** `--standalone __app` nasza biblioteka **nie będzie** mogła niczego udostępniać dla przeglądarki!

**Można** zmienić nazwę `__app` na własną, tylko należy pamiętać o pliku `./web/app.browserify.html` gdzie jest ona używana!

**§2 ⇒** Wpliku `./rollup.config.js` znajdują się ustawiania:

```
rollupConfig = {
    output: {
        name: '__app' //<- nazwa biblioteki
    }
}
```

Jest to ta sama sytuacja co w punkcie pierwszym **§1** tylko że dla aplikacji budującej [rollupjs.org](https://rollupjs.org) i musimy pamiętać o pliku `./web/app.rollup.html`
