const path = require('path');

function getFullName() {
    return path.resolve(__dirname, 'build/Release/addon.node');
}

function getNative() {
    try {
        return require(getFullName());
    } catch (_error) {
        throw new Error('This version of "native app" is not compatible with your Node.js build:\n\n' + _error.toString());
    }
}

module.exports = getNative();