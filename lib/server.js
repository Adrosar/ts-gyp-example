#!/usr/bin/env node

"use strict";

const express = require('express');
const serveIndex = require('serve-index');
const { projectResolve } = require('./helpers');

const staticOptions = {
    etag: true,
    maxAge: 60
};

const app = express();

function serverBuild(port) {

    app.use('/app.browserify.js', function(_req, _res, _next) {
        _res.sendFile(projectResolve('build/app.browserify.js'));
    });

    app.use('/test.browserify.js', function(_req, _res, _next) {
        _res.sendFile(projectResolve('build/test.browserify.js'));
    });

    app.use('/app.rollup.js', function(_req, _res, _next) {
        _res.sendFile(projectResolve('build/app.rollup.js'));
    });

    app.use('/test.rollup.js', function(_req, _res, _next) {
        _res.sendFile(projectResolve('build/test.rollup.js'));
    });

    app.use('/~typedoc', express.static(projectResolve('typedoc'), staticOptions));

    app.use('/', express.static(projectResolve('web'), staticOptions));
    app.use('/', serveIndex(projectResolve('web'), {
        'icons': true
    }));

    app.listen(port, function() {
        console.log('Example app listening on port ' + port + ' !');
    })
}

serverBuild(60188);