#!/usr/bin/env node

"use strict";

const symlinkDir = require('symlink-dir');
const helpers = require('./helpers');

function link(_src, _dest) {
    symlinkDir(helpers.projectResolve(_src), helpers.projectResolve(_dest));
}

function main() {
    const argv = process.argv || [];

    let src = "";
    let dest = "";

    for (let i = 0; i < argv.length; i++) {
        const first = argv[i];
        const second = argv[i + 1];

        if (first === '--src') {
            if (typeof second === 'string' && second.length > 0) {
                src = second;
            }
        }

        if (first === '--dest') {
            if (typeof second === 'string' && second.length > 0) {
                dest = second;
            }
        }
    }

    if (src && dest) {
        link(src, dest);
    }
}

if (require.main === module) {
    main();
}